<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'PT KMWI';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            $this->_login();
        }
    }

    private function _login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->db->get_where('user', ['username' => $username])->row_array();
        if ($user) {
            //usernya ada
            if (md5($password) === $user['password']) {
                $data = [
                    'username' => $user['username'],
                    'departemen' => $user['departemen']
                ];
                $this->session->set_userdata($data);
                if ($user['departemen'] == 1) {
                    redirect('admin');
                } else if ($user['departemen'] == 2) {
                    redirect('user/production');
                } else if ($user['departemen'] == 3) {
                    redirect('user/foreman');
                } else if ($user['departemen'] == 4) {
                    redirect('user/ppic');
                } else {
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert
            alert-danger" role="alert">Wrong password!</div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert
            alert-danger" role="alert">Username is not registered!</div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('departemen');

        $this->session->set_flashdata('message', '<div class="alert
        alert-success" role="alert">You have been logged out!</div>');
        redirect('auth');
    }
}
