<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Menu Manajemen';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('menu', 'Menu', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('user_menu', ['menu' => $this->input->post('menu')]);
            $this->session->set_flashdata('message', '<div class="alert
            alert-success" role="alert">New Menu Added</div>');
            redirect('menu');
        }
    }


    public function submenu()
    {
        $data['title'] = 'Sub Menu';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active'),
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert
            alert-success" role="alert">New Submenu Added</div>');
            redirect('menu/submenu');
        }
    }

    public function datauser()
    {
        $data['title'] = 'Data User';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->load->model('User_model', 'user');

        $data['dataUser'] = $this->user->getUser();

        $data['nm_departemen'] = $this->db->get('user_departemen')->result_array();

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('departemen', 'Departemen', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menuproduction/datauser', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'nama' => $this->input->post('nama'),
                'departemen' => $this->input->post('departemen'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
            ];

            $this->db->insert('user', $data);
            $this->session->set_flashdata('message', '<div class="alert
            alert-success" role="alert">New User Added</div>');
            redirect('menu/datauser');
        }
    }

    public function dataproduk()
    {
        $data['title'] = 'Data Produk ';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();


        $data['dataProduk'] = $this->db->get('data_produk')->result_array();


        $this->form_validation->set_rules('nama_produk', 'Nama_produk', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menuproduction/dataproduk', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'nama_produk' => $this->input->post('nama_produk')
            ];

            $this->db->insert('data_produk', $data);
            $this->session->set_flashdata('message', '<div class="alert
            alert-success" role="alert">New Product Added</div>');
            redirect('menuproduction/dataproduk');
        }
    }

    public function datastandar()
    {
        $data['title'] = 'Data Standar OEE';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();


        $data['dataStandar'] = $this->db->get('data_standaroee')->result_array();


        $this->form_validation->set_rules('tgl_standar', 'Tgl_standar', 'required');
        $this->form_validation->set_rules('availability', 'Availability', 'required');
        $this->form_validation->set_rules('performance', 'Performance', 'required');
        $this->form_validation->set_rules('quality', 'Quality', 'required');
        $this->form_validation->set_rules('oee', 'Oee', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menuproduction/datastandar', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'tgl_standar' => $this->input->post('tgl_standar'),
                'availability' => $this->input->post('availability'),
                'performance' => $this->input->post('performance'),
                'quality' => $this->input->post('quality'),
                'oee' => $this->input->post('oee')
            ];

            $this->db->insert('data_standaroee', $data);
            $this->session->set_flashdata('message', '<div class="alert
            alert-success" role="alert">New Product Added</div>');
            redirect('menu/datastandar');
        }
    }

    public function dataoee()
    {
        $data['title'] = 'Data Input OEE';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->load->model('Oee_model', 'oee');

        $data['dataOEE'] = $this->oee->getOee();



        $this->form_validation->set_rules('jam_kerja', 'Jam Kerja', 'required');
        $this->form_validation->set_rules('breakdown', 'Breakdown', 'required');
        $this->form_validation->set_rules('setup', 'Setup', 'required');
        $this->form_validation->set_rules('run_time', 'Run Time', 'required');
        $this->form_validation->set_rules('ideal_runtime', 'Ideal Run Time', 'required');
        $this->form_validation->set_rules('bad_count', 'Bad Count', 'required');
        $this->form_validation->set_rules('good_count', 'Good Count', 'required');
        $this->form_validation->set_rules('total_count', 'Total Count', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menuproduction/dataoee', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [

                'id_produksi' => 1,
                'jam_kerja' => $this->input->post('jam_kerja'),
                'breakdown' => $this->input->post('breakdown'),
                'setup' => $this->input->post('setup'),
                'run_time' => $this->input->post('run_time'),
                'ideal_runtime' => $this->input->post('ideal_runtime'),
                'bad_count' => $this->input->post('bad_count'),
                'good_count' => $this->input->post('good_count'),
                'total_count' => $this->input->post('total_count')
            ];

            $this->db->insert('input_oee', $data);
            $this->session->set_flashdata('message', '<div class="alert
            alert-success" role="alert">New Variabel OEE Added</div>');
            redirect('menu/dataoee');
        }
    }

    public function hasiloee()
    {
        $data['title'] = 'Data Hasil Perhitungan OEE';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();

        $data['dataOEE'] = $this->db->get('input_oee')->result_array();

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menuproduction/hasiloee', $data);
            $this->load->view('templates/footer');
        } else {
        }
    }

    public function inputproduksi()
    {
        $data['title'] = 'Laporan Produksi';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();


        $data['dataLap'] = $this->db->get('laporan_produksi')->result_array();


        $this->form_validation->set_rules('id_produksi', 'Id_produksi', 'required');
        $this->form_validation->set_rules('user_id', 'User_id', 'required');
        $this->form_validation->set_rules('no_produk', 'No_produk', 'required');
        $this->form_validation->set_rules('tgl_produksi', 'Tgl_produksi', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menuproduction/inputproduksi', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'id_produksi' => $this->input->post('id_produksi'),
                'user_id' => $this->input->post('user_id'),
                'no_produk' => $this->input->post('no_produk'),
                'tgl_produksi' => $this->input->post('tgl_produksi'),
            ];

            $this->db->insert('laporan_produksi', $data);
            $this->session->set_flashdata('message', '<div class="alert
            alert-success" role="alert">Added New Laporan Produksi</div>');
            redirect('menu/inputproduksi');
        }
    }

    public function hasilproduksi()
    {
        $data['title'] = 'Data Hasil Laporan Produksi';
        $data['user'] = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();

        $data['dataLap'] = $this->db->get('laporan_produksi')->result_array();
        $data['dataOEE'] = $this->db->get('input_oee')->result_array();

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menuproduction/hasilproduksi', $data);
            $this->load->view('templates/footer');
        } else {
        }
    }
}
