<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">PT KMWI</h1>

    <div class="card mb-3">
        <img src="<?= base_url('assets/img/kmwi.jpg'); ?>" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">PT Kreasi Mandiri Wintor Indonesia</h5>
            <p class="card-text">Jalan Pahlawan Km.1,5, Karang Asem Tim. Kec. Citeureup. Bogor, Jawa Barat.</p>
            <p class="card-text">Telepon : (021) 8752489</p>


        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->