<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Hitung OEE</h1>


        <div class="row">
            <div class="col-lg">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>

                <?= $this->session->flashdata('message'); ?>

                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Nomor OEE</th>
                            <!-- <th scope="col">Tanggal</th>
                            <th scope="col">ID Produksi</th>
                            <th scope="col">Jam Kerja</th>
                            <th scope="col">Breakdown</th>
                            <th scope="col">Setup</th>
                            <th scope="col">Run Time</th>
                            <th scope="col">Ideal Run Time</th>
                            <th scope="col">Bad Count</th>
                            <th scope="col">Good Count</th>
                            <th scope="col">Total Count</th> -->
                            <th scope="col">Availability</th>
                            <th scope="col">Performance</th>
                            <th scope="col">Quality</th>
                            <th scope="col">OEE</th>
                            <!-- <th scope="col">Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($dataOEE as $do) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th>
                                <!-- <td><?= $do['tgl_oee']; ?></td>
                                <td><?= $do['id_produksi']; ?></td>
                                <td><?= $do['jam_kerja']; ?></td>
                                <td><?= $do['breakdown']; ?></td>
                                <td><?= $do['setup']; ?></td>
                                <td><?= $do['run_time']; ?></td>
                                <td><?= $do['ideal_runtime']; ?></td>
                                <td><?= $do['bad_count']; ?></td> -->
                                <!-- <td><?= $do['good_count']; ?></td> -->
                                <!-- <td><?= $do['total_count']; ?></td> -->

                                <td><?= $a = round((($do['jam_kerja'] - ($do['breakdown'] + $do['setup']) / $do['jam_kerja']) * 00.1), 1.0); ?></td>
                                <td><?= $p = round((($do['jam_kerja'] / ($do['run_time']) / $do['ideal_runtime']) * 100), 1.0); ?></td>
                                <td><?= $q = round((($do['total_count'] - $do['bad_count']) / $do['ideal_runtime'] * 100), 1.0); ?></td>
                                <td><?= $o = round(($a * $p * $q * 00.1), 0); ?></td>
                                <!-- <td>
                                    <a href="" class="badge badge-success">Cetak</a>
                                </td> -->
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>