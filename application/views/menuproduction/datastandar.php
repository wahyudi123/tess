<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Data Standar OEE</h1>




        <div class="row">
            <div class="col-lg">
                <?= form_error('dataStandar', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                <?= $this->session->flashdata('message'); ?>

                <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newDataStandarModal">Tambah Standar OEE</a>
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Nomor Standar OEE</th>
                            <th scope="col">Tanggal Standar OEE</th>
                            <th scope="col">Availability</th>
                            <th scope="col">Performance</th>
                            <th scope="col">Quality</th>
                            <th scope="col">OEE</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($dataStandar as $ds) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th>
                                <td><?= $ds['tgl_standar']; ?></td>
                                <td><?= $ds['availability']; ?></td>
                                <td><?= $ds['performance']; ?></td>
                                <td><?= $ds['quality']; ?></td>
                                <td><?= $ds['oee']; ?></td>
                                <td>
                                    <a href="" class="badge badge-success">edit</a>
                                    <a href="" class="badge badge-danger">delete</a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="newDataStandarModal" tabindex="-1" role="dialog" aria-labelledby="newDataStandarModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newDataStandarModalLabel">Tambah Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?= base_url('menu/datastandar'); ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="date" class="form-control" id="tgl_standar" name="tgl_standar" placeholder="Tanggal Standar OEE">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="availability" name="availability" placeholder="Standar Avilability">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="performance" name="performance" placeholder="Standar Performance">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="quality" name="quality" placeholder="Standar Quality">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="oee" name="oee" placeholder="Standar OEE">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->