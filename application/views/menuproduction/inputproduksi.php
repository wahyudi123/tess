<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Input Laporan Produksi</h1>


        <div class="row">
            <div class="col-lg">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>

                <?= $this->session->flashdata('message'); ?>

                <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newInputProduksiModal">Tambah Laporan Produksi</a>
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">ID Produksi</th>
                            <th scope="col">User ID</th>
                            <th scope="col">Nomor Produk</th>
                            <th scope="col">Tanggal Produksi</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($dataLap as $dl) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th>
                                <td><?= $dl['user_id']; ?></td>
                                <td><?= $dl['no_produk']; ?></td>
                                <td><?= $dl['tgl_produksi']; ?></td>
                                <td>
                                    <a href="" class="badge badge-success">edit</a>
                                    <a href="" class="badge badge-danger">delete</a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="newInputProduksiModal" tabindex="-1" role="dialog" aria-labelledby="newInputProduksiModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newInputProduksiModalLabel">Input Laporan Produksi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?= base_url('menu/inputproduksi'); ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="id_produksi" name="id_produksi" placeholder="ID Produksi">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="user_id" name="user_id" placeholder="ID User">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="no_produk" name="no_produk" placeholder="Nomor Produk">
                            </div>
                            <div class="form-group">
                                <input type="date" class="form-control" id="tgl_produksi" name="tgl_produksi" placeholder="Tanggal Produksi">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->