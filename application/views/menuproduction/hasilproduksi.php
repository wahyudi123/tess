<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Hasil Laporan Produksi</h1>


        <div class="row">
            <div class="col-lg">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>

                <?= $this->session->flashdata('message'); ?>

                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">ID Produksi</th>
                            <th scope="col">User ID</th>
                            <th scope="col">Nomor Produk</th>
                            <th scope="col">Tanggal Produksi</th>
                            <th scope="col">Jam Kerja</th>
                            <th scope="col">Breakdown</th>
                            <th scope="col">Setup</th>
                            <th scope="col">Run Time</th>
                            <th scope="col">Ideal Run Time</th>
                            <th scope="col">Bad Count</th>
                            <th scope="col">Good Count</th>
                            <th scope="col">Total Count</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($dataLap as $dl) : ?>
                            <?php foreach ($dataOEE as $do) : ?>
                                <tr>
                                    <th scope="row"><?= $i; ?></th>
                                    <td><?= $dl['user_id']; ?></td>
                                    <td><?= $dl['no_produk']; ?></td>
                                    <td><?= $dl['tgl_produksi']; ?></td>
                                    <td><?= $do['jam_kerja']; ?></td>
                                    <td><?= $do['breakdown']; ?></td>
                                    <td><?= $do['setup']; ?></td>
                                    <td><?= $do['run_time']; ?></td>
                                    <td><?= $do['ideal_runtime']; ?></td>
                                    <td><?= $do['bad_count']; ?></td>
                                    <td><?= $do['good_count']; ?></td>
                                    <td><?= $do['total_count']; ?></td>
                                    <td>
                                        <a href="" class="badge badge-success">Cetak</a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>