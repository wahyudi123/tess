<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Input Variabel OEE</h1>


        <div class="row">
            <div class="col-lg">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>

                <?= $this->session->flashdata('message'); ?>

                <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newdataoeeModal">Tambah Variabel OEE</a>
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Nomor OEE</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">ID Produksi</th>
                            <th scope="col">Jam Kerja</th>
                            <th scope="col">Breakdown</th>
                            <th scope="col">Setup</th>
                            <th scope="col">Run Time</th>
                            <th scope="col">Ideal Run Time</th>
                            <th scope="col">Bad Count</th>
                            <th scope="col">Good Count</th>
                            <th scope="col">Total Count</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($dataOEE as $do) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th>
                                <td><?= $do['tgl_oee']; ?></td>
                                <td><?= $do['id_produksi']; ?></td>
                                <td><?= $do['jam_kerja']; ?></td>
                                <td><?= $do['breakdown']; ?></td>
                                <td><?= $do['setup']; ?></td>
                                <td><?= $do['run_time']; ?></td>
                                <td><?= $do['ideal_runtime']; ?></td>
                                <td><?= $do['bad_count']; ?></td>
                                <td><?= $do['good_count']; ?></td>
                                <td><?= $do['total_count']; ?></td>

                                <td>
                                    <a href="" class="badge badge-success">edit</a>
                                    <a href="" class="badge badge-danger">delete</a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="newdataoeeModal" tabindex="-1" role="dialog" aria-labelledby="newdataoeeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newdataoeeModalLabel">Input Variabel OEE</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?= base_url('menu/dataoee'); ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="date" class="form-control" id="tgl_oee" name="tgl_oee" placeholder="Tanggal">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="jam_kerja" name="jam_kerja" placeholder="Jam Kerja">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="breakdown" name="breakdown" placeholder="Breakdown">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="setup" name="setup" placeholder="Setup">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="run_time" name="run_time" placeholder="Run Time">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="ideal_runtime" name="ideal_runtime" placeholder="Ideal Run Time">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="bad_count" name="bad_count" placeholder="Bad Count">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="good_count" name="good_count" placeholder="Good Count">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="total_count" name="total_count" placeholder="Total Count">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->