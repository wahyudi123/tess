<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Oee_model extends CI_Model
{
    public function getOee()
    {
        $query = "SELECT * 
    FROM `input_oee`
    ORDER BY `id_oee` ASC
    ";

        return $this->db->query($query)->result_array();
    }
}
