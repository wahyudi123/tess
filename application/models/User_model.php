<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function getUser()
    {
        $query = "SELECT `user`.*, `user_departemen`.`nm_departemen`
    FROM `user` JOIN `user_departemen`
    ON `user`.`departemen` = `user_departemen`.`id`
    ORDER BY `user`.`user_id` ASC
    ";

        return $this->db->query($query)->result_array();
    }
}
